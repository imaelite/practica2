<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Practica 3';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= $titulo ?></h1>

        <p><?= $desc ?></p>
    </div>

    <div class="body-content">

        <div class="row">

            <?php
            echo "El número de empleados es: " . $datos;
            ?>

        </div>

    </div>
</div>
