<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Practica 3';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= $titulo ?></h1>

        <p><?= $desc ?></p>
    </div>

    <div class="body-content">

        <div class="row">

            <?php
            foreach ($datos as $indice => $valor) {
                echo "Codigo de empleado: " . $valor->emp_no . ", Apellido: " . $valor->apellido . ", Oficio: " . $valor->oficio .  ", Departamento: " . $valor->dept_no . "<br>";
            }
            ?>

        </div>

    </div>
</div>
