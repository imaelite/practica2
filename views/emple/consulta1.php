<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Practica 2';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= $titulo ?></h1>

        <p><?= $desc ?></p>
    </div>

    <div class="body-content">

        <div class="row">

                <?php
                foreach ($datos as $indice=>$valor){
                /*    echo $indice.":".$valor->deptNo->dnombre."<br>";      */
                    
                    echo "Apellido: ".$valor->apellido.
                         ", Oficio: " .$valor->oficio.
                         ", Direccion: ".$valor->dir. 
                         ", Fecha de alta: ".$valor->fecha_alt.
                         ", Salario: ".$valor->salario.
                                 ", Comisión: ".$valor->comision.
                                 ", Departamento: ".$valor->dept_no .".<br>";
                    
                }
            
                ?>
            
        </div>

    </div>
</div>
