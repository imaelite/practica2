<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Practica 2';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= $titulo ?></h1>

        <p><?= $desc ?></p>
    </div>

    <div class="body-content">

        <div class="row">

                <?php
                foreach ($datos as $indice=>$valor){

                    echo "Nº de Departamento: ".$valor->dept_no." Nombre: ".$valor->dnombre.
                         ", Localización: ".$valor->loc.".<br>";
                      
                    
                }
            
                ?>
            
        </div>

    </div>
</div>
