<?php

namespace app\controllers;

use Yii;
use app\models\Emple;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EmpleController implements the CRUD actions for Emple model.
 */
class EmpleController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Emple models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Emple::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Emple model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Emple model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Emple();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->emp_no]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Emple model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->emp_no]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Emple model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Emple model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Emple the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Emple::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /* Consultas ------------------------------------------------------------------------------------------- */

    public function actionConsulta1() {
        $datos = Emple::find()
                ->all();

        $titulo = 'Consulta 1';

        $desc = 'Listar todos los empleados';

        return $this->render('consulta1', [
                    'datos' => $datos,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta3() {
        $datos = Emple::find()->select('apellido,oficio')->distinct()->all();


        $titulo = 'Consulta 3';

        $desc = 'Listar oficio y el apellido del empleado';

        return $this->render('consulta3', [
                    'datos' => $datos,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta4() {

        $titulo = 'Consulta 4';

        $desc = 'Listar los apellidos de los empleados del departameneto 20, 02 y 30 y su salario es mayor de 1500';

        $datos = Emple::find()
                ->where(["and", ["in", "dept_no", [20, 30]], "salario>1500"])
                ->all();

        /*   $datos = Emple::find()
          ->where("salario>1500 and dept_no in (20,30)")
          ->all(); */

        return $this->render('consultas', [
                    'datos' => $datos,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta5() {


        /* $datos = Emple::find()->select('dept_no')
          ->where("salario>1500 and apellido like 'S%'")
          ->all(); */
        $datos = Emple::find()
                ->where(['or', ['like', 'apellido', 'S%'], 'salario>1500'])
                ->all();

        $titulo = 'Consulta 5';

        $desc = 'Listar los departaments de los empleados cuyo salario sea mayor que 1500 o su apellido comienze por s.';

        return $this->render('consultas', [
                    'datos' => $datos,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta6() {



        $datos = Emple::find()->count();




        $titulo = 'Consulta 6';

        $desc = 'Número de empleados que hay';

        return $this->render('consulta6', [
                    'datos' => $datos,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta7() {


        $datos = Emple::find()
                ->orderBy('apellido')
                ->all();

        $titulo = 'Consulta 7';

        $desc = 'Datos de los empleados por apellido de forma ascendente ';

        return $this->render('consulta7', [
                    'datos' => $datos,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta8() {


        $datos = Emple::find()
                ->orderBy(['apellido' => SORT_DESC])
                ->all();

        $titulo = 'Consulta 8';

        $desc = 'Datos de los empleados por apellido de forma descendente ';

        return $this->render('consulta8', [
                    'datos' => $datos,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta10() {


        $datos = Emple::find()
                ->orderBy(['dept_no' => SORT_DESC])
                ->all();

        $titulo = 'Consulta 10';

        $desc = 'Datos de los empleados ordenados por departamento de forma descendente ';

        return $this->render('consulta10', [
                    'datos' => $datos,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta11() {


        $datos = Emple::find()
                ->orderBy(['dept_no' => SORT_DESC, 'oficio' => SORT_ASC])
                ->all();

        $titulo = 'Consulta 11';

        $desc = 'Datos de los empleados ordenados por departamento de forma descendente y por oficio asscendente';

        return $this->render('consulta11', [
                    'datos' => $datos,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta12() {


        $datos = Emple::find()
                ->orderBy(['dept_no' => SORT_DESC, 'apellido' => SORT_ASC])
                ->all();

        $titulo = 'Consulta 12';

        $desc = 'Datos de los empleados ordenados por departamento de forma descendente y por apellido asscendente';

        return $this->render('consulta12', [
                    'datos' => $datos,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta13() {


        $datos = Emple::find()->select('emp_no,salario')
                ->where('salario>2000')
                ->all();

        $titulo = 'Consulta 13';

        $desc = 'Códigos de los empleados cuyo salario sea mayor que 2000';

        return $this->render('consulta13', [
                    'datos' => $datos,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta14() {


        $datos = Emple::find()->select('emp_no,salario,apellido')
                ->where('salario<2000')
                ->all();

        $titulo = 'Consulta 14';

        $desc = 'Códigos de los empleados, apellido y salario cuyo salario sea mayor que 2000';

        return $this->render('consulta14', [
                    'datos' => $datos,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta15() {


        $datos = Emple::find()->select('emp_no,salario,apellido')
                ->where(['Between', 'salario', '1500', '2500'])
                ->all();

        $titulo = 'Consulta 15';

        $desc = 'Listar empleados cuyo salario esté entre 1500 y 2500';

        return $this->render('consulta15', [
                    'datos' => $datos,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta16() {


        $datos = Emple::find()
                ->where(['oficio' => 'ANALISTA'])
                ->all();

        $titulo = 'Consulta 16';

        $desc = 'Listar empleados cuyo cuyo oficio sea analista';

        return $this->render('consulta16', [
                    'datos' => $datos,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta17() {


        $datos = Emple::find()
                ->where(['and', ['oficio' => 'ANALISTA'], ['>', 'salario', 2000]])
                ->all();

        $titulo = 'Consulta 17';

        $desc = 'Listar empleados cuyo cuyo oficio sea analista';

        return $this->render('consulta17', [
                    'datos' => $datos,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta18() {


        $datos = Emple::find()
                ->where(['dept_no' => '20'])
                ->all();

        $titulo = 'Consulta 18';

        $desc = 'Sleeccionar apellido y oficio de los empleados del departamento 20';

        return $this->render('consulta18', [
                    'datos' => $datos,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta19() {


        $datos = Emple::find()
                        ->where(['oficio' => 'vendedor'])->count()
        ;

        $titulo = 'Consulta 19';

        $desc = 'Contar el numero de empleados cuyo oficio sea vendedor';

        return $this->render('consulta19', [
                    'datos' => $datos,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta20() {


        $datos = Emple::find()
                        ->where(['or','left(apellido,1)="m"','left(apellido,1)="n"'])
                        ->orderBy(['apellido' => SORT_ASC])->all();
        ;

        $titulo = 'Consulta 20';

        $desc = 'Mostrar los empleados cuyo apellido empieze por m o por n ordenados por apellido';

        return $this->render('consulta20', [
                    'datos' => $datos,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

}
